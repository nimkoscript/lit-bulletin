const express = require('express');
const app = express();
const fs = require('fs');
const passport = require('passport');
const mongoose = require('mongoose');
const Strategy = require('passport-litauth').Strategy;
var session = require('express-session');
var Filter = require('bad-words'),
    filter = new Filter();
var MongoDBStore = require('connect-mongodb-session')(session);
require('dotenv').config()
const port = process.env.LIT_BULLETIN_PORT ? process.env.LIT_BULLETIN_PORT : 5001;
var store = new MongoDBStore({
	uri: process.env.MONGODB_HOST,
	collection: 'sessions'
});

mongoose.connect(process.env.MONGODB_HOST);
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
let Message
db.once('open', function() {
	const messageSchema = new mongoose.Schema({
		message: String,
		author: Object,
		createdAt: Date
	});
	Message = mongoose.model('message', messageSchema);
});

app.use(express.static('public'));
app.use(express.json());
app.use(require('cookie-parser')());
app.use(express.urlencoded({ extended: true }));
app.set("view engine", "ejs");
app.use(require('express-session')({ secret: process.env.SESSION_SECRET, resave: true, saveUninitialized: false, store: store, cookie: { maxAge: 1000 * 60 * 60 * 24 * 7 } }));

passport.serializeUser(function(user, done) {
	done(null, user);
  });
passport.deserializeUser(function(obj, done) {
	done(null, obj);
});

passport.use(new Strategy({
	clientID: process.env.CLIENT_ID,
	clientSecret: process.env.CLIENT_SECRET,
	callbackURL: 'https://nimkoscript.litdevs.org/callback',
	scope: ["identify"]
}, function(accessToken, refreshToken, profile, done) {
	done(null, profile)
}));

app.use(passport.initialize());
app.use(passport.session());

app.get("/", (req, res) => {
	Message.find({}, (err, messages) => {
		let user = req.isAuthenticated() ? req.user : null;
		if (err) res.sendStatus(500);
		res.render("index", { messages: messages, user});
	})
})

app.post("/", (req, res) => {
	if (!req.isAuthenticated()) return res.redirect("/litauth");
	if (!req.body.message) return res.sendStatus(400);
	const message = new Message({
		message: filter.clean(req.body.message),
		author: {
			id: req.user._id,
			name: req.user.username
		},
		createdAt: new Date()
	})
	message.save((err) => {
		if (err) res.sendStatus(500);
		res.redirect("/");
	})
})

app.get('/litauth', passport.authenticate('litauth'));
app.get('/callback', passport.authenticate('litauth', {
    failureRedirect: '/?login=failed'
}), function(req, res) {
    res.redirect('/') // Successful auth
});

app.listen(5001, () => {
	console.log(`HTTP listening on port ${port}`)
})
